Matomo
=========

Deploy Matomo (including redis and mariadb) using docker.

Requirements
------------

You need docker and the docker python library installed on your host.

Role Variables
--------------

Look into the defaults file.

License
-------

AGPLv3
